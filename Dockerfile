FROM node:latest
RUN mkdir -p /usr/src/authProject
WORKDIR /usr/src/authProject
COPY package.json /usr/src/authProject/
RUN npm install
RUN npm install -g concurrently
RUN npm uninstall bcrypt
RUN npm i bcrypt
COPY . /usr/src/authProject
EXPOSE 8080
CMD ["concurrently","npm:dev"]