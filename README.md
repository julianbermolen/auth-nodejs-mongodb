# Proyecto de Authorización y Authenticación

El siguiente es un proyecto realizado con el fin de poder realizar una autenticación y authorización  de manera simple via servicios Rest, utilizando jsonwebToken.

# Tecnologia

* NodeJS
* MongoDB
* Docker

# Iniciar proyecto Docker

```bash
    docker-compose build
    docker-compose up
```
El archivo dockerFile ejecuta el paso a paso para levantar la aplicación, instalando NPM y las dependencias propias del package.json